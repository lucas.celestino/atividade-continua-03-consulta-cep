package com.example.cepconsultation

import android.os.AsyncTask
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import org.json.JSONException
import org.json.JSONObject
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.URL

class MainActivity : AppCompatActivity() {
    private lateinit var cepEditText: EditText
    private lateinit var searchButton: Button
    private lateinit var resultTextView: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        cepEditText = findViewById(R.id.cepEditText)
        searchButton = findViewById(R.id.searchButton)
        resultTextView = findViewById(R.id.resultTextView)

        searchButton.setOnClickListener {
            val cep = cepEditText.text.toString().trim()
            if (isValidCEP(cep)) {
                GetAddressTask().execute(cep)
            } else {
                showToast("CEP inválido")
            }
        }
    }

    private fun isValidCEP(cep: String): Boolean {
        return cep.matches(Regex("\\d{8}"))
    }

    private fun showToast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

    private inner class GetAddressTask : AsyncTask<String, Void, String>() {
        override fun doInBackground(vararg params: String): String {
            val cep = params[0]
            val url = "https://viacep.com.br/ws/$cep/json/"

            try {
                val apiUrl = URL(url)
                val connection = apiUrl.openConnection() as HttpURLConnection
                connection.requestMethod = "GET"

                val responseCode = connection.responseCode
                if (responseCode == HttpURLConnection.HTTP_OK) {
                    val inputStream = connection.inputStream
                    val reader = BufferedReader(InputStreamReader(inputStream))
                    val response = StringBuilder()
                    var line: String?

                    while (reader.readLine().also { line = it } != null) {
                        response.append(line)
                    }

                    reader.close()
                    inputStream.close()

                    return response.toString()
                } else {
                    throw IOException("Erro na consulta. Código de resposta: $responseCode")
                }
            } catch (e: IOException) {
                e.printStackTrace()
                throw IOException("Erro ao conectar-se ao servidor")
            }
        }

        override fun onPostExecute(result: String) {
            super.onPostExecute(result)

            try {
                val json = JSONObject(result)

                if (json.has("erro")) {
                    showToast("CEP não encontrado")
                    return
                }

                val logradouro = json.getString("logradouro")
                val complemento = json.getString("complemento")
                val bairro = json.getString("bairro")
                val localidade = json.getString("localidade")
                val uf = json.getString("uf")
                val codigoIbge = json.getString("ibge")
                val codigoGia = json.getString("gia")
                val prefixoTelefone = json.getString("ddd")
                val codigoSiafi = json.getString("siafi")

                val address = "Logradouro: $logradouro\n" +
                        "Complemento: $complemento\n" +
                        "Bairro: $bairro\n" +
                        "Localidade: $localidade\n" +
                        "UF: $uf\n" +
                        "Código IBGE: $codigoIbge\n" +
                        "Código GIA: $codigoGia\n" +
                        "Prefixo Telefone (DDD): $prefixoTelefone\n" +
                        "Código SIAFI: $codigoSiafi"

                resultTextView.text = address
            } catch (e: JSONException) {
            }
        }
    }
}